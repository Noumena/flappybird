﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BirdController : MonoBehaviour
{
    [SerializeField]
    private Vector2 MoveVector;

    [SerializeField]
    private ObstacleMover obstacleMover;

    [SerializeField]
    private AudioClip moveUpClip;

    [SerializeField]
    private AudioClip deadClip;

    [SerializeField]
    private AudioClip scoreClip;

    [SerializeField]
    private Text scoreText;

    [SerializeField]
    private Text highscoreText;

    [SerializeField]
    private Text startText;

    [SerializeField]
    private Text gameOverText;

    private bool isMoving = false, isStart = false, isGameOver = false;
    private Rigidbody2D rigidbody;
    private AudioSource audioSource;

    private int score = 0;

    private const string HighscoreKey = "HighScore";

    private void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        audioSource = GetComponent<AudioSource>();
        SetScoreText(0);
        Time.timeScale = 0f;

        highscoreText.text = PlayerPrefs.GetInt(HighscoreKey, 0).ToString();
    }

    private void Update()
    { 
        if (isStart)
        {
            PlayerInput();
        }
        else
        {
            ShowStartMenu();
        }

        if(isGameOver)
        {
            ReStart();
        }
    }

    private static void ReStart()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void PlayerInput()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            MoveUp();

        if (Input.GetMouseButtonDown(0))
            MoveUp();
    }

    private void ShowStartMenu()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
        {
            if (startText)
                startText.gameObject.SetActive(false);

            Time.timeScale = 1f;
            isStart = true;
        }
    }

    private void MoveUp()
    {
        if (isMoving)
            return;

        rigidbody.velocity = MoveVector;
        audioSource.PlayOneShot(moveUpClip);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Score"))
        {
            audioSource.PlayOneShot(scoreClip);
            score++;
            SetScoreText(score);
        }
    }

    private void SetScoreText(int score)
    {
        scoreText.text = score.ToString();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (isMoving)
            return;

        isMoving = true;
        isGameOver = true;

        audioSource.PlayOneShot(deadClip);

        if(score > PlayerPrefs.GetInt(HighscoreKey, 0))
           PlayerPrefs.SetInt(HighscoreKey, score);

        obstacleMover.Stop();

        ShowGameOverMenu();
    }

    private void ShowGameOverMenu()
    {
        if (gameOverText)
            gameOverText.gameObject.SetActive(true);              
    }
}
