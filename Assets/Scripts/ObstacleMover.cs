﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleMover : MonoBehaviour
{

    [SerializeField]
    Vector3 moveSpeedPerSec;

    [SerializeField]
    private bool isStop = false;

    [SerializeField]
    private GameObject obstaclePrefab;

    [SerializeField]
    private float spawnTime;

    [SerializeField]
    private float minY;

    [SerializeField]
    private float maxY;

    [SerializeField]
    private Vector3 objectPoolPos;

    [SerializeField]
    private int obstaclePoolSize = 6;

    private GameObject[] obstacles;

    private void Start()
    {
        SetObstaclePoolArrays();
        StartCoroutine(SpawnObstacle());
    }

    private IEnumerator SpawnObstacle()
    {
        yield return new WaitForSeconds(3f);
        int currentPoolSize = 0;

        while (!isStop)
        {
            //spawn obstacle
            GameObject go = obstacles[currentPoolSize];

            //Set parent
            go.transform.parent = transform;

            //Random Y
            float y = UnityEngine.Random.Range(minY, maxY);

            //Set position
            go.transform.position = new Vector3(12, y, 0);

            //Random gap
            //float gap = Random.Range(12f, 20f);
            float halfGap = UnityEngine.Random.Range(6f, 7f);

            go.transform.GetChild(0).localPosition = new Vector3(0f, -halfGap, 0f);
            go.transform.GetChild(1).localPosition = new Vector3(0f, halfGap, 0f);

            currentPoolSize++;

            if(currentPoolSize >= obstaclePoolSize)
            {
                currentPoolSize = 0;
            }
            
            yield return new WaitForSeconds(3f);
        }
    }

    private void Update()
    {
        if (isStop)
            return;

        Vector3 moveSpeedPerFrame = Time.deltaTime * moveSpeedPerSec;
        transform.Translate(moveSpeedPerFrame);
    }

    private void SetObstaclePoolArrays()
    {
        obstacles = new GameObject[obstaclePoolSize];

        for(int i =0; i < obstaclePoolSize; i++)
        {
            obstacles[i] = Instantiate(obstaclePrefab, objectPoolPos, Quaternion.identity);
        }
    }
    
    public void Stop()
    {
        isStop = true;
    }
}
